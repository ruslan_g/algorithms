#include <combinationgenerator.h>
#include <heapsort.h>

#include <chrono>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include "quicksort.h"

bool compare(int a, int b)
{
    return b <= a;
}

void testHeapSort()
{
    {
        HeapSort<SortDirection::asc> qsAsc;
        int ar[] = { 4, 1, 3, 2, 16, 9 };

        std::ifstream infile("test.txt");
        std::string line;
        while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            //iss >> ar[0] >> ar[1] >> ar[2] >> ar[3] >> ar[4] >> ar[5];
            auto from = std::begin(ar);
            auto to = std::end(ar);
            qsAsc.sort(ar, (sizeof ar) / sizeof(int));
            if (!std::is_sorted(from, to))
            {
                throw 123;
            }
        }
        qsAsc.print(ar);
    }

    {
        HeapSort<SortDirection::desc> qsDesc;
        int ar[] = { 4, 1, 3, 2, 16, 9 };

        std::ifstream infile("test.txt");
        std::string line;
        while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            //iss >> ar[0] >> ar[1] >> ar[2] >> ar[3] >> ar[4] >> ar[5];
            auto from = std::begin(ar);
            auto to = std::end(ar);
            qsDesc.sort(ar, (sizeof ar) / sizeof(int));
            if (!std::is_sorted(from, to, compare))
            {
                throw 123;
            }
        }
    }

    std::cout << "Sorting test succeeded" << std::endl;

}

void generateFileWithPermutations()
{
    CombinationGenerator cg;

    const std::vector<int> input{ 0, 1, 2, 3, 4, 5 };
    const auto before = std::chrono::system_clock::now();
    const auto& combinations = cg.generateArrangements(input, 6);
    const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - before);
    std::cout << "Generating of permutations took " << duration.count() / 1000.0 << "s" << std::endl;

    std::ofstream out("test.txt");
    for (const auto& combination : combinations)
    {
        for (const auto& v : combination)
        {
            out << v << ' ';
        }
        out << std::endl;
    }
}

//void testIncreasing()
//{
//    HeapSort<SortDirection::desc> qsAsc;
//    int ar[] = { 4, 1, 3, 2, 16, 9 };
//    qsAsc.sort(ar, (sizeof ar) / sizeof(int));
//    qsAsc.increase(ar, 3, 10);
//}

void testPopMax()
{
    HeapSort<SortDirection::desc> qsAsc;
    int ar[] = { 2, 1, 4, 3, 9, 8, 0 };
    qsAsc.sort(ar, (sizeof ar) / sizeof(int));
    int d = qsAsc.popMax(ar);
}

int main()
{
    QuickSort q;
    int arr[]{ 3, 1, 2, 5, 6, 0, 1, 7, 9 };
    q.sort(arr, 7);
    /*int arr[]{ 8, 7, 7, 7, 9 };
    q.sort(arr, 5);*/
    for(auto i:arr)
    {
        std::cout << i << " ";
    }
    //testPopMax();
    //testIncreasing();
    //generateFileWithPermutations();
    //testHeapSort();
    return 0;
}
