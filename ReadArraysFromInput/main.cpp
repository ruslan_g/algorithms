#include <iostream>
#include <vector>
#include <iterator>
#include <string>
#include <sstream>
#include <limits>
#include <ctime>
#include <chrono>

void main()
{
    auto microseconds_since_epoch = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    int n, q;
    std::cin >> n >> q;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::vector<std::vector<int>> vectors(n);

    std::string line;
    for (auto& v : vectors)
    {
        std::getline(std::cin, line);
        std::istringstream stream(line);
        int number;
        stream >> number;
        while (stream >> number)
            v.push_back(number);
    }

    for(int i = 0; i < q; ++i)
    {
        int row{};
        int column{};
        std::cin >> row >> column;
        std::cout << vectors[row][column] << std::endl;
    }
}
