#include <vector>
#include <string>
#include <set>

using namespace std;

// Complete the matchingStrings function below.
vector<int> matchingStrings(vector<string> strings, vector<string> queries)
{
    vector<int> result;
    multiset<string> stringSet;
    for (const auto& str : strings)
    {
        stringSet.insert(str);
    }
    for (const auto& query : queries)
    {
        result.push_back(stringSet.count(query));
    }
    return result;

}

int main()
{

    vector<string> strings{ "abcde",
    "sdaklfj",
    "asdjf",
    "na",
    "basdn",
    "sdaklfj",
    "asdjf",
    "na",
    "asdjf",
    "na",
    "basdn",
    "sdaklfj",
    "asdjf" };

    vector<string> queries{ "abcde", "sdaklfj", "asdjf", "na", "basdn" };

    vector<int> res = matchingStrings(strings, queries);

    return 0;
}
