#include <vector>
#include <iostream>

int getMinRefillNumber(const std::vector<int>& distances, int volume)
{
    size_t currentPosition = 0;
    size_t lastPosition = 0;
    int numberOfFillings{};

    while (currentPosition < distances.size() - 1)
    {
        while(currentPosition < (distances.size() - 1) && (distances[currentPosition +1] - distances[lastPosition] <= volume))
        {
            ++currentPosition;
        }
        if (currentPosition == lastPosition)
        {
            return -1;
        }
        if (currentPosition < (distances.size() - 1))
        {
            ++numberOfFillings;
        }
        lastPosition = currentPosition;
    }
    return numberOfFillings;
}

void main()
{
    std::vector<int> distances{ 0, 250, 375, 550, 750, 950 };
    int result = getMinRefillNumber(distances, 400);
    std::cout << result;
}