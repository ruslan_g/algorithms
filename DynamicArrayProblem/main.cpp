// https://www.hackerrank.com/challenges/dynamic-array/problem

#include <vector>
#include <iostream>

std::vector<int> dynamicArray(int n, std::vector<std::vector<int>> queries)
{
    std::vector<int> result;
    std::vector<std::vector<int>> sequences(n);
    int lastAnswer{};
    for (const auto& query : queries)
    {
        int seqIndex = (query[1] ^ lastAnswer) % n;
        if (query[0]==1)
            sequences[seqIndex].push_back(query[2]);
        if (query[0] == 2)
        {
            lastAnswer = sequences[seqIndex][query[2] % sequences[seqIndex].size()];
            result.push_back(lastAnswer);
        }
    }
    return result;
}

void main()
{
    std::vector<std::vector<int>> input
    {
        {1, 0, 5},
        {1, 1, 7 },
        {1, 0, 3},
        {2, 1, 0},
        {2, 1, 1}
    };
    dynamicArray(2, input);
}
