#include "adjacencymatrix.h"
#include "graph.h"

void main()
{
    AdjacencyMatrix matrix(6);
    matrix.addEdge(Vertex{ 0 }, Vertex{ 1 });
    matrix.addEdge(Vertex{ 0 }, Vertex{ 3 });
    matrix.addEdge(Vertex{ 0 }, Vertex{ 4 });
    matrix.addEdge(Vertex{ 1 }, Vertex{ 2 });
    matrix.addEdge(Vertex{ 3 }, Vertex{ 2 });
    matrix.addEdge(Vertex{ 4 }, Vertex{ 2 });
    matrix.addEdge(Vertex{ 4 }, Vertex{ 5 });
    matrix.print();
    matrix.getAllPaths();
    //Graph graph(10);
    //graph.addNode("A");
    return;
}
