#pragma once
#include <vector>

#include "vertex.h"
struct Node
{
    int vertexId;
    int pathWeight;
    Node* prev;
};

class AdjacencyMatrix
{
    std::vector<bool> _visited;
    std::vector<Node*> _nodes;
    std::vector<std::vector<int>> _vertexArray{};
    void visit(int vertexId, Node* n);

public:
    AdjacencyMatrix(int vertexCount);
    void addEdge(const Vertex& u, const Vertex& v, int weight = 1, bool oriented = false);
    void print();
    void getAllPaths();
};

