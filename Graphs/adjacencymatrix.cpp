#include "adjacencymatrix.h"

#include <iomanip>
#include <iostream>

#include "vertex.h"

AdjacencyMatrix::AdjacencyMatrix(int vertexCount)
{
    _vertexArray.resize(vertexCount);
    _visited.resize(vertexCount);
    for (auto& u : _vertexArray)
    {
        u.resize(vertexCount);
    }

}

void AdjacencyMatrix::addEdge(const Vertex& u, const Vertex& v, int weight, bool oriented)
{
    _vertexArray[u.id][v.id] = weight;
    if (!oriented)
    {
        _vertexArray[v.id][u.id] = weight;
    }
}

void AdjacencyMatrix::print()
{
    for (const auto u : _vertexArray)
    {
        for (const auto v : u)
        {
            std::cout << std::setw(3) << v;
        }
        std::cout << std::endl;
    }
}


void AdjacencyMatrix::visit(int vertexId, Node* n)
{
    _visited[vertexId] = true;
    int index = 0;
    for (const auto pathWeight : _vertexArray[vertexId])
    {
        if (pathWeight !=0 && !_visited[index])
        {
            Node* newNode = new Node{ index, pathWeight, n };
            visit(index, newNode);
        }
        ++index;
    }
    _nodes.push_back(n);

    _visited[vertexId] = false;
}

void AdjacencyMatrix::getAllPaths()
{
    for (auto i = 0; i < _vertexArray.size(); ++i)
    {
        Node* n = new Node{ i, 0,nullptr };
        visit(i, n);
    }

    for (auto* node: _nodes)
    {
        Node* ptr = node;
        do
        {
            std::cout << ptr->vertexId;
            if (ptr->prev)
            {
                std::cout << "-";
            }
        } while ((ptr = ptr->prev) != nullptr);
        std::cout << std::endl;
    }
}
