#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <list>

typedef struct node 
{
    node() = default;
    node(const node&) = default;
    node& operator=(const node&) = default;

    double freq;
    char data;
    node * left;
    node * right;

} node;

class Comparator
{
public:
    bool operator()(node* n1, node* n2) 
    {
        return n1->freq > n2->freq;
    };
};
node* huffman_hidden(const std::string& s)
{
    std::multiset<char> characters;
    for(char ch:s)
    {
        characters.insert(ch);
    }

    std::list<node*> nodes;
    auto total = double(characters.size());
    for (auto each = characters.begin(); each != characters.end();)
    { 
        node* n = new node;
        n->data = *each;
        n->freq = characters.count(*each)/total;
        n->left = n->right = nullptr;

        nodes.push_back(n);
        each = characters.upper_bound(*each);
    }

    nodes.sort(Comparator());
    while (nodes.size() > 1)
    {
        auto left = nodes.front();
        nodes.pop_front();
        auto right = nodes.front();
        nodes.pop_front();
        node* n = new node;
        n->freq = left->freq + right->freq;
        n->left = left;
        n->right = right;
        nodes.push_front(n);
        nodes.sort(Comparator());

    }

    return nodes.front();
}

void decode_huff(node* root, std::string s) 
{
    node* next = root;
    for (const auto& ch: s)
    {
        next = (ch == '0') ? next->left : next->right;
        if (next->left == nullptr && next->right == nullptr)
        {
            std::cout << next->data;
            next = root;
        }
    }
}
void main()
{
    std::string s;
    //std::cin >> s;

    node * tree = huffman_hidden("abracadabra");
    std::string code = "1001011";
    std::map<char, std::string>mp;

    //print_codes_hidden(tree, code, mp);

    /*std::string coded;

    for (int i = 0; i < s.length(); i++) {
        coded += mp[s[i]];
    }*/

    decode_huff(tree, code);
}