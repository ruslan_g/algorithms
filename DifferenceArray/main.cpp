#include <iostream>
#include <iomanip>
#include <vector>

std::vector<int> toDifferenceArray(const std::vector<int>& input)
{
    std::vector<int> output;
    output.push_back(input.front());
    for (int i = 1; i < input.size(); ++i)
    {
        output.push_back(input[i] - input[i-1]);
    }
    return output;
}

std::vector<int> fromDifferenceArray(const std::vector<int>& input)
{
    std::vector<int> output;
    output.push_back(input.front());
    for (int i = 1; i < input.size(); ++i)
    {
        output.push_back(input[i] + output[i-1]);
    }
    return output;
}

void print(const std::vector<int>& input)
{
    for (auto val : input)
    {
        std::cout << std::setw(4) << val;
    }
    std::cout << std::endl;
}

void main()
{
    std::vector<int> input{ 2, 3, 4, 5, 6, 7, 8, 9 };

    std::cout << "Initial input:" << std::endl;
    print(input);

    std::vector<int> output = toDifferenceArray(input);
    std::cout << "Difference array:" << std::endl;
    print(output);


    std::vector<int> output2 = fromDifferenceArray(output);
    std::cout << "Array after restoring:" << std::endl;
    print(output2);

    std::cout << "Update difference array +10 for [1] and -10 for [4]:" << std::endl;
    output[1] += 10;
    output[4] -= 10;
    print(output);

    output2 = fromDifferenceArray(output);
    std::cout << "Array after restoring:" << std::endl;
    print(output2);
}
