#include <vector>
#include <iostream>

struct Node
{
    Node()
    {
        left = right = nullptr;
    }

    int data;
    Node* left;
    Node* right;
};

Node* buildTreeFromVectorOfVectors(std::vector<std::vector<int>> indices)
{
    std::vector<Node*> nodes;

    Node* firstNode = new Node();
    firstNode->data = 1;
    nodes.push_back(firstNode);

    for (const auto& indPair : indices)
    {
        Node* nodeWithoutChildren = nullptr;
        for (Node* n : nodes)
        {
            if (!n->left && !n->right)
            {
                nodeWithoutChildren = n;
                break;
            }
        }

        nodeWithoutChildren->left = indPair[0] != -1 ? new Node():nullptr;
        if (indPair[0] != -1)
        {
            nodeWithoutChildren->left->data = indPair[0];
        }

        nodeWithoutChildren->right = indPair[1] != -1 ? new Node() : nullptr;
        if (indPair[1] != -1)
        {
            nodeWithoutChildren->right->data = indPair[1];
        }

        nodes.push_back(nodeWithoutChildren->left);
        nodes.push_back(nodeWithoutChildren->right);
    }
    return firstNode;
}

void traversInOrderRecur(Node* tree, std::vector<Node*>& traversedNodes)
{
    if (tree->left)
    {
        traversInOrderRecur(tree->left, traversedNodes);
    }

    traversedNodes.push_back(tree);
    if (tree->right)
    {
        traversInOrderRecur(tree->right, traversedNodes);
    }
}

std::vector<Node*> traversInOrder(Node* tree)
{
    std::vector<Node*> nodes;
    traversInOrderRecur(tree, nodes);
    return nodes;
}

void swap(int k, struct Node* root, int level)
{
    if (root != nullptr)
    {
        if (level%k == 0)
        {
            auto temp = root->left;
            root->left = root->right;
            root->right = temp;
        }
        swap(k, root->left, level + 1);
        swap(k, root->right, level + 1);
    }
}

void main()
{
    std::vector<std::vector<int>> indices{ {2, 3}, {4, 5}, {6, 7}, {8, 9} };
    Node* tree = buildTreeFromVectorOfVectors(indices);
    auto nodes = traversInOrder(tree);
    for (const auto node : nodes)
    {
        std::cout << node->data << " ";
    }
    std::cout << std::endl;
    swap(2, tree, 0);
    auto nodes2 = traversInOrder(tree);
    for (const auto node : nodes2)
    {
        std::cout << node->data << " ";
    }

}