#include <fstream>
#include <iterator>
#include "quicksort.h"
#include <algorithm>
#include <iostream>

#include <string>
#include <sstream>
#include "heapsort.h"

int main()
{
    HeapSort qs;
    int ar[] = { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };

    
    std::ifstream infile("test.txt");
    std::string line;
    while (std::getline(infile, line))
    {
        std::istringstream iss(line);
        //iss >> ar[0] >> ar[1] >> ar[2] >> ar[3] >> ar[4] >> ar[5];
        auto from = std::begin(ar);
        auto to = std::end(ar);
        qs.sort(ar, (sizeof ar) / sizeof(int));
        if (!std::is_sorted(from, to))
            throw 123;
    }
	return 0;

}
