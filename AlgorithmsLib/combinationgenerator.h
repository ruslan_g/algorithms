﻿#pragma once
#include <algorithm>
#include <vector>
#include <iostream>

class __declspec(dllexport) CombinationGenerator
{
    void permutate(const std::vector<int>& combinationValues, 
        std::vector<std::vector<int>>& outputCombinations,
        int level, 
        int combinationLength,
        int& row) const;

public:

    std::vector<std::vector<int>> generateArrangements(const std::vector<int>& input, int combinationLength) const;
};
