#pragma once
#include "enums.h"
#include <iostream>

#include <utility>

class HeapSortCommon
{
protected:
    unsigned int m_size{};
public:

    int getLeft(int i)
    {
        return (i << 1) + 1; // left = 2*i + 1 
    }

    int getRight(int i)
    {
        return (i << 1) + 2; // left = 2*i + 1 
    }
    int getParent(int i)
    {
        return (i - 1) >> 1;
    }
    void print(int* arr)
    {
        for (int i = 0; i <= m_size / 2 - 1; ++i)
        {
            auto left = getLeft(i);
            auto right = getRight(i);

            std::cout << arr[i] << "(" 
                << (left < m_size ? arr[left]:-1111)
                << ", " 
                << (right < m_size ? arr[right]:-1111) << ")"
                << std::endl;
        }
    }
};

template <SortDirection direction = SortDirection::asc> class __declspec(dllexport) HeapSort: public HeapSortCommon
{
public:
    HeapSort() = default;

    ~HeapSort() = default;

    void sort(int* arr, int length)
    {
        m_size = length;
        // Build heap (rearrange array) 
        for (int i = length / 2 - 1; i >= 0; i--)
        {
            heapify(arr, length, i);
        }

        // One by one extract an element from heap 
        for (int i = length - 1; i >= 0; i--)
        {
            // Move current root to end 
            std::swap(arr[0], arr[i]);

            // call max heapify on the reduced heap 
            heapify(arr, i, 0);
        }
    }

    int popMax(int arr[])
    {
        if (m_size < 1)
            throw "heap underflow";

        int max = arr[0];
        arr[0] = arr[m_size - 1];
        --m_size;
        heapify(arr, m_size, 0);
        return max;
    }

    void increase(int arr[], int i, int val)
    {
        if (val < arr[i])
        {    
            throw "new key is smaller than current key"; 
        }

        arr[i] = val;
        while ( i > 0 && arr[getParent(i)] < arr[i])
        {
            std::swap(arr[i], arr[getParent(i)]);
            i = getParent(i);
        }
    }

    void heapify(int arr[], int length, int rootIndex)
    {
        int indexOfLargest = rootIndex; // Initialize largest as root 
        int leftIndex = getLeft(rootIndex);
        int rightIndex = getRight(rootIndex);

        // If left child exists and is larger than root 
        if (leftIndex < length && arr[leftIndex] > arr[indexOfLargest])
        {
            indexOfLargest = leftIndex;
        }

        // If right child  exists and is larger than largest so far 
        if (rightIndex < length && arr[rightIndex] > arr[indexOfLargest])
        {
            indexOfLargest = rightIndex;
        }

        // If largest is not root 
        if (indexOfLargest != rootIndex)
        {
            std::swap(arr[rootIndex], arr[indexOfLargest]);

            // Recursively heapify the affected sub-tree 
            heapify(arr, length, indexOfLargest);
        }
    }
};

template <> class __declspec(dllexport) HeapSort<SortDirection::desc>: public HeapSortCommon
{
public:
    HeapSort() = default;


    ~HeapSort() = default;

    void sort(int* arr, int length)
    {
        m_size = length;
        // Build heap (rearrange array) 
        for (int i = length / 2 - 1; i >= 0; i--)
        {
            heapify(arr, length, i);
        }

        // One by one extract an element from heap 
        for (int i = length - 1; i >= 0; i--)
        {
            // Move current root to end 
            std::swap(arr[0], arr[i]);

            // call max heapify on the reduced heap 
            heapify(arr, i, 0);
        }
    }

    int popMax(int arr[])
    {
        if (m_size < 1)
            throw "heap underflow";

        int max = arr[0];
        arr[0] = arr[m_size - 1];
        --m_size;
        heapify(arr, m_size, 0);
        return max;
    }

    void increase(int arr[], int i, int val)
    {
        if (val < arr[i])
        {
            throw "new key is smaller than current key";
        }

        arr[i] = val;
        while (i > 0 && arr[getParent(i)] < arr[i])
        {
            std::swap(arr[i], arr[getParent(i)]);
            i = getParent(i);
        }
    }

    void heapify(int arr[], int length, int rootIndex)
    {
        int indexOfLeast = rootIndex; // Initialize Least as root 
        int leftIndex = getLeft(rootIndex);
        int rightIndex = getRight(rootIndex);

        // If left child exists and is larger than root 
        if (leftIndex < length && arr[leftIndex] < arr[indexOfLeast])
        {
            indexOfLeast = leftIndex;
        }

        // If right child  exists and is larger than Least so far 
        if (rightIndex < length && arr[rightIndex] < arr[indexOfLeast])
        {
            indexOfLeast = rightIndex;
        }

        // If Least is not root 
        if (indexOfLeast != rootIndex)
        {
            std::swap(arr[rootIndex], arr[indexOfLeast]);

            // Recursively heapify the affected sub-tree 
            heapify(arr, length, indexOfLeast);
        }
    }

};
