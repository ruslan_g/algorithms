#pragma once
#include <utility>
class __declspec(dllexport) QuickSort
{
public:
	QuickSort();
	~QuickSort() = default;

    int getPartitionPositionLeft(int* arr, int left, int right) const;

    int getPartitionPositionRight(int* arr, int left, int right) const;

    void sortRecur(int* arr, int left, int right);

    void sort(int* arr, int length);
};

