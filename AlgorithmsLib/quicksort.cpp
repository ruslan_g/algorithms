#include "quicksort.h"

QuickSort::QuickSort()
= default;


int QuickSort::getPartitionPositionLeft(int* arr, int left, int right) const
{
    // arr[left] is pivot
    int leftIterator = left + 1;
    int rightIterator = right;
    while (true)
    {
        while ((arr[leftIterator] <= arr[left]) && (leftIterator < right))
        {
            ++leftIterator;
        }

        while (arr[rightIterator] > arr[left]) // redundant (rightIterator > left)
        {
            --rightIterator;
        }

        if (leftIterator >= rightIterator)
            break;

        std::swap(arr[leftIterator], arr[rightIterator]);
    }
    std::swap(arr[left], arr[rightIterator]);

    return rightIterator;
}

int QuickSort::getPartitionPositionRight(int* arr, int left, int right) const
{
    // arr[right] is pivot
    int leftIterator = left;
    int rightIterator = right - 1;
    while (true)
    {
        while (arr[rightIterator] >= arr[right] && (rightIterator > left)) // redundant (rightIterator > left)
        {
            --rightIterator;
        }
        while (arr[leftIterator] < arr[right])
        {
            ++leftIterator;
        }


        if (leftIterator >= rightIterator)
            break;

        std::swap(arr[leftIterator], arr[rightIterator]);
    }
    std::swap(arr[right], arr[leftIterator]);

    return leftIterator;
}

void QuickSort::sortRecur(int* arr, int left, int right)
{
    if (left >= right)
    {
        return;
    }

    const int pivotPosition = getPartitionPositionLeft(arr, left, right);
    sortRecur(arr, left, pivotPosition - 1);
    sortRecur(arr, pivotPosition + 1, right);
}

void QuickSort::sort(int* arr, int length)
{
    sortRecur(arr, 0, length - 1);
}
