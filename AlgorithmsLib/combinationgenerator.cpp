﻿#include "combinationgenerator.h"

void CombinationGenerator::permutate(const std::vector<int>& combinationValues, std::vector<std::vector<int>>& outputCombinations, int level, int combinationLength, int& row) const
{
    if (level == combinationLength)
    {
        if (row < outputCombinations.size() - 1)
        {
            ++row;
            for (int i = 0; i < combinationLength; ++i)
            {
                outputCombinations[row][i] = outputCombinations[row - 1][i];
            }
        }
        return;
    }

    for (const auto& cv: combinationValues)
    {
        outputCombinations[row][level] = cv;
        permutate(combinationValues, outputCombinations, level + 1, combinationLength, row);
    }
}

std::vector<std::vector<int>> CombinationGenerator::generateArrangements(const std::vector<int>& input, int combinationLength) const
{
    std::vector<std::vector<int>> outputCombinations(pow(input.size(), combinationLength), std::vector<int>(combinationLength));
    int row = 0;
    permutate(input, outputCombinations, 0, combinationLength, row);

    return outputCombinations;
}