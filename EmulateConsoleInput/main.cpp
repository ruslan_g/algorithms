#include <iostream>
#include <sstream>

void main()
{
    std::streambuf* orig = std::cin.rdbuf();
    std::istringstream input("whatever");
    std::cin.rdbuf(input.rdbuf());
    // tests go here

    std::string s;
    std::cin >> s;
    std::cin.rdbuf(orig);
}
