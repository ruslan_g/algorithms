#include <iostream>
#include "dnc.h"

int main()
{
	DnC dnq;
	int values[]{ -2, -5, 6, -2, -3, 1, 5, -6 };
	auto res = dnq.findMaxSum(values, 0, 7);
	std::cout << "subarray ["<< res.from << "..." << res.to << "], sum = " << res.result;
	return 0;
}
