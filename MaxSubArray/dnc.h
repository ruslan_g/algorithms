#pragma once

struct SubArrayResult
{
	int from;
	int to;
	int result;
};
class DnC
{
public:
	DnC() = default;
	~DnC() = default;
	SubArrayResult findMaxSum(int* arr, int left, int right);
	SubArrayResult findCrossMaxSum(int* arr, int left, int middle, int right) const;
};

