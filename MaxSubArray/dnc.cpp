#include <algorithm>
#include "dnc.h"
#include <iostream>

SubArrayResult DnC::findMaxSum(int* arr, int left, int right)
{
	if (left == right)
	{
		std::cout << "left == right at index " << left << ", returning " << arr[left] << std::endl;
		return { left, right,arr[left] };
	}

	int middle = (left + right) >> 1;

	auto leftSum = findMaxSum(arr, left, middle);
	auto rightSum = findMaxSum(arr, middle + 1, right);

	auto crossSum = findCrossMaxSum(arr, left, middle, right);

	SubArrayResult result{};
	if (leftSum.result > rightSum.result)
	{
		result = SubArrayResult{ leftSum.from, leftSum.to, leftSum.result };
	}
	else
	{
		result = SubArrayResult{ rightSum.from, rightSum.to, rightSum.result };
	}

	if (result.result < crossSum.result)
	{
		result = SubArrayResult{ crossSum.from, crossSum.to, crossSum.result };
	}

	std::cout << "max(leftSum, rightSum, crossSumm["<< left<< "..." << right <<"]): max(" << leftSum.result << "," << rightSum.result << "," << crossSum.result << ") -> " << result.result <<  std::endl;

	return result;
}

SubArrayResult DnC::findCrossMaxSum(int* arr, int left, int middle, int right) const
{
	int leftSum = INT_MIN;
	int rightSum = INT_MIN;
	int leftBound=middle, rightBound= middle + 1;
	int sum = 0;

	for(int i = middle; i >= left; --i)
	{
		sum += arr[i];
		if (sum > leftSum)
		{
			leftBound = i;
			leftSum = sum;
		}
	}

	sum = 0;
	for (int i = middle + 1; i <= right; ++i)
	{
		sum += arr[i];
		if (sum > rightSum)
		{
			rightBound = i;
			rightSum = sum;
		}
	}

	return { leftBound, rightBound, leftSum + rightSum };
}
