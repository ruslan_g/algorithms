#include <vector>
#include <iostream>
#include "binarytree.h"
#include <algorithm>

bool testInOrder()
{
    std::vector<int> values{ 12, 9, 6, 3, 0, 10, 8, 5, 2, 1, 15, 18, 13, 14, 17, 19 };
    BinaryTree* bt = new BinaryTree();
    bt->initializeFromVector(values);

    auto nodes = bt->traverseInOrder();

    std::vector<int> actual;
    for(const auto& n:nodes)
    {
        actual.push_back(n->data);
    }

    std::vector<int> expected{0, 1, 2, 3, 5, 6, 8, 9, 10, 12, 13, 14, 15, 17, 18, 19};

    return expected == actual;
}

bool testPreOrder()
{
    std::vector<int> values{ 12, 9, 6, 3, 0, 10, 8, 5, 2, 1, 15, 18, 13, 14, 17, 19 };
    BinaryTree* bt = new BinaryTree();
    bt->initializeFromVector(values);

    auto nodes = bt->traversePreOrder();

    std::vector<int> actual;
    for (const auto& n : nodes)
    {
        actual.push_back(n->data);
    }

    std::vector<int> expected{ 12, 9, 6, 3, 0, 2, 1, 5, 8, 10, 15, 13, 14, 18, 17, 19 };

    return expected == actual;
}

bool testPostOrder()
{
    std::vector<int> values{ 12, 9, 6, 3, 0, 10, 8, 5, 2, 1, 15, 18, 13, 14, 17, 19 };
    BinaryTree* bt = new BinaryTree();
    bt->initializeFromVector(values);

    auto nodes = bt->traversePostOrder();

    std::vector<int> actual;
    for (const auto& n : nodes)
    {
        actual.push_back(n->data);
    }

    std::vector<int> expected{ 1, 2, 0, 5, 3, 8, 6, 10, 9, 14, 13, 17, 19, 18, 15, 12 };

    return expected == actual;
}
void main()
{
    bool res = testInOrder();
    res = testPreOrder();
    res = testPostOrder();
    std::vector<int> values{ 12, 9, 6, 3, 0, 10, 8, 5, 2, 1, 15, 18, 13, 14, 12, 17, 19 };
    BinaryTree* bt = new BinaryTree();
    bt->initializeFromVector(values);

    auto nodes = bt->traversePreOrder();
    for(const auto node: nodes)
    {
        std::cout << node->data << " ";
    }


}