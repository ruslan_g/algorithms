﻿#include "binarytree.h"

Node* BinaryTree::getParent(Node* tree, int value)
{
    if (tree->data > value)
    {
        if (tree->left)
            return getParent(tree->left, value);
    }
    else
    {
        if (tree->right)
            return getParent(tree->right, value);
    }
    return tree;
}

Node* BinaryTree::getRoot() const
{
    return m_root;
}

void BinaryTree::initializeFromVector(const std::vector<int> values)
{
    m_root = new Node();
    m_root->data = values[0];

    for (auto it = values.begin() + 1; it != values.end(); ++it)
    {
        Node* parent = getParent(m_root, *it);
        if (parent->data > *it)
        {
            parent->left = new Node();
            parent->left->data = *it;
        }
        else
        {
            parent->right = new Node();
            parent->right->data = *it;
        }
    }
}

void BinaryTree::traverseInOrderRecur(Node* tree, std::vector<Node*>& traversedNodes)
{
    if (tree->left)
    {
        traverseInOrderRecur(tree->left, traversedNodes);
    }

    traversedNodes.push_back(tree);

    if (tree->right)
    {
        traverseInOrderRecur(tree->right, traversedNodes);
    }
}

std::vector<Node*> BinaryTree::traverseInOrder()
{
    std::vector<Node*> nodes;
    traverseInOrderRecur(m_root, nodes);
    return nodes;
}

void BinaryTree::traversePreOrderRecur(Node* tree, std::vector<Node*>& traversedNodes)
{
    traversedNodes.push_back(tree);
    if (tree->left)
    {
        traversePreOrderRecur(tree->left, traversedNodes);
    }

    if (tree->right)
    {
        traversePreOrderRecur(tree->right, traversedNodes);
    }
}

std::vector<Node*> BinaryTree::traversePostOrder()
{
    std::vector<Node*> nodes;
    traversePostOrderRecur(m_root, nodes);
    return nodes;
}

void BinaryTree::traversePostOrderRecur(Node* tree, std::vector<Node*>& traversedNodes)
{
    if (tree->left)
    {
        traversePostOrderRecur(tree->left, traversedNodes);
    }

    if (tree->right)
    {
        traversePostOrderRecur(tree->right, traversedNodes);
    }
    traversedNodes.push_back(tree);
}

std::vector<Node*> BinaryTree::traversePreOrder()
{
    std::vector<Node*> nodes;
    traversePreOrderRecur(m_root, nodes);
    return nodes;
}
