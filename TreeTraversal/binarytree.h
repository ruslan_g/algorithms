﻿#pragma once
#include <vector>

struct Node
{
    int data;
    Node* left;
    Node* right;
};

class BinaryTree
{
    Node* m_root = nullptr;
    Node* getParent(Node* tree, int value);

    void traverseInOrderRecur(Node* tree, std::vector<Node*>& traversedNodes);
    void traversePreOrderRecur(Node* tree, std::vector<Node*>& traversedNodes);
    void traversePostOrderRecur(Node* tree, std::vector<Node*>& traversedNodes);

public:
    Node* getRoot() const;
    void initializeFromVector(const std::vector<int> values);
    std::vector<Node*> traverseInOrder();
    std::vector<Node*> traversePreOrder();
    std::vector<Node*> traversePostOrder();
};
