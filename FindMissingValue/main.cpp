#include <iostream>

int searchFirstMissing(int ar[], int size)
{
    if (size < 2)
    {
        return -1;
    }

    if (size == 2)
    {
        return ar[1] - ar[0] == 1 ? -1 : 0;
    }
    int start = 0, end = size - 1;
    int mid;
    bool shrinked{};
    while ((end - start) > 1) 
    {
        mid = (start + end) / 2;
        if ((ar[start] - start) != (ar[mid] - mid))
        {
            end = mid;
            --mid;
            shrinked = true;
        }
        else if ((ar[end] - end) != (ar[mid] - mid))
        {
            shrinked = true;
            start = mid;
        }
        if (!shrinked)
        {
            return -1;
        }
        shrinked = false;
    }
    return mid;
}


void main()
{
    int ar[] = { 0, 1, 2, 3, 4, 5, 7 };
    int size = sizeof(ar) / sizeof(ar[0]);
    int position = searchFirstMissing(ar, size);
    if (position != -1)
    {
        std::cout << "Missing number:" << ar[position]+1;
    }
    else
    {
        std::cout << "not found";
    }
}